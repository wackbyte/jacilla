#![deny(clippy::pedantic, clippy::nursery)]

use {
	crate::markov::Chain,
	jacildata::Data,
	rayon::iter::{ParallelBridge, ParallelIterator},
	std::{
		fs::File,
		io::{self, Read},
		num::NonZeroUsize,
		path::PathBuf,
		process,
	},
	structopt::StructOpt,
	thiserror::Error,
};

mod markov;

#[derive(Error, Debug)]
enum Error {
	#[error("io error: {0}")]
	Io(#[from] io::Error),
	#[error("serde error: {0}")]
	Serde(#[from] serde_json::Error),
}

/// Generate new Jacilla quotes using a Markov chain.
#[derive(StructOpt, Clone, Debug)]
struct Opt {
	/// The path to the database.
	#[structopt(short, long)]
	data: PathBuf,
	/// The order of the Markov chain.
	#[structopt(short, long, default_value = "1")]
	order: NonZeroUsize,
	/// The number of quotes to generate.
	#[structopt(short, long)]
	count: usize,
	/// Whether or not to filter out duplicates.
	#[structopt(short = "f", long)]
	no_duplicates: bool,
}

fn run() -> Result<(), Error> {
	let opt = Opt::from_args();

	let quotes = {
		let mut file = File::open(opt.data)?;
		let mut text = String::new();
		file.read_to_string(&mut text)?;
		let data = serde_json::from_str::<Data>(&text)?;

		data.streams
			.iter()
			.filter_map(|stream| stream.quotes.clone().map(IntoIterator::into_iter))
			.flatten()
			.chain(data.tweets.iter().cloned())
			.collect::<Vec<String>>()
	};

	let mut chain = Chain::<String>::of_order(opt.order);
	for quote in &quotes {
		chain.feed_str(quote);
	}

	let generator = if opt.no_duplicates {
		chain
			.str_iter_for(opt.count)
			.par_bridge()
			.filter(|fake| !quotes.contains(fake))
			.collect::<Vec<String>>()
	} else {
		chain
			.str_iter_for(opt.count)
			.par_bridge()
			.collect::<Vec<String>>()
	};
	println!("{}", serde_json::to_string_pretty(&generator)?);

	Ok(())
}

fn main() {
	if let Err(error) = run() {
		eprintln!("{error}");
		process::exit(1);
	} else {
		process::exit(0);
	}
}
