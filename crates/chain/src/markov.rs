//! A generic [Markov chain](https://en.wikipedia.org/wiki/Markov_chain) for almost any type.
//! In particular, elements of the chain must be `Eq`, `Hash`, and `Clone`.
//!
//! # Examples
//!
//! ```
//! use markov::Chain;
//!
//! let mut chain = Chain::new();
//! chain.feed_str("I like cats and I like dogs.");
//! println!("{}", chain.generate_str());
//! ```
//!
//! ```
//! use markov::Chain;
//!
//! let mut chain = Chain::new();
//! chain.feed(vec![1u8, 2, 3, 5]).feed([3u8, 9, 2]);
//! println!("{:?}", chain.generate());
//! ```

use {
	rand::{thread_rng, Rng},
	serde::{Deserialize, Serialize},
	std::{
		borrow::ToOwned,
		collections::{hash_map::Entry, HashMap},
		hash::Hash,
		iter::Map,
		num::NonZeroUsize,
	},
};

/// The definition of all types that can be used in a `Chain`.
pub trait Chainable: Eq + Hash + Clone {}

impl<T> Chainable for T where T: Eq + Hash + Clone {}

type Token<T> = Option<T>;

/// A generic [Markov chain](https://en.wikipedia.org/wiki/Markov_chain) for almost any type.
/// In particular, elements of the chain must be `Eq`, `Hash`, and `Clone`.
#[derive(PartialEq, Debug, Serialize, Deserialize)]
pub struct Chain<T>
where
	T: Chainable,
{
	map: HashMap<Vec<Token<T>>, HashMap<Token<T>, usize>>,
	order: NonZeroUsize,
}

impl<T> Default for Chain<T>
where
	T: Chainable,
{
	fn default() -> Self {
		Self::new()
	}
}

impl<T> Chain<T>
where
	T: Chainable,
{
	/// Constructs a new Markov chain.
	pub fn new() -> Self {
		Self::of_order(NonZeroUsize::new(1).unwrap())
	}

	/// Creates a new Markov chain of the specified order. The order is the number of previous
	/// tokens to use for each mapping in the chain. Higher orders mean that the generated text
	/// will more closely resemble the training set. Increasing the order can yield more realistic
	/// output, but typically at the cost of requiring more training data.
	pub fn of_order(order: NonZeroUsize) -> Self {
		Self {
			map: {
				let mut map = HashMap::new();
				map.insert(vec![None; order.get()], HashMap::new());
				map
			},
			order,
		}
	}

	/// Determines whether or not the chain is empty. A chain is considered empty if nothing has
	/// been fed into it.
	pub fn is_empty(&self) -> bool {
		self.map[&vec![None; self.order.get()]].is_empty()
	}

	/// Feeds the chain a collection of tokens. This operation is `O(n)` where `n` is the number of
	/// tokens to be fed into the chain.
	pub fn feed<S>(&mut self, tokens: S) -> &mut Self
	where
		S: AsRef<[T]>,
	{
		let tokens = tokens.as_ref();
		if tokens.is_empty() {
			return self;
		}

		let mut food = vec![None; self.order.get()];
		food.extend(tokens.iter().map(|token| Some(token.clone())));
		food.push(None);
		for p in food.windows(self.order.get() + 1) {
			self.map
				.entry(p[0..self.order.get()].to_vec())
				.or_insert_with(HashMap::new);
			self.map
				.get_mut(&p[0..self.order.get()].to_vec())
				.unwrap()
				.add(p[self.order.get()].clone());
		}

		self
	}

	/// Generates a collection of tokens from the chain. This operation is `O(mn)` where `m` is the
	/// length of the generated collection, and `n` is the number of possible states from a given
	/// state.
	pub fn generate(&self) -> Vec<T> {
		let mut output = Vec::new();
		let mut current = vec![None; self.order.get()];
		loop {
			let next = self.map[&current].next();

			current = current[1..self.order.get()].to_vec();
			current.push(next.clone());
			if let Some(next) = next {
				output.push(next);
			}

			if current[self.order.get() - 1].is_none() {
				break;
			}
		}

		output
	}

	/// Generates a collection of tokens from the chain, starting with the given token. This
	/// operation is O(mn) where m is the length of the generated collection, and n is the number
	/// of possible states from a given state. This returns an empty vector if the token is not
	/// found.
	pub fn generate_from_token(&self, token: T) -> Vec<T> {
		let mut current = vec![None; self.order.get() - 1];
		current.push(Some(token.clone()));
		if !self.map.contains_key(&current) {
			return Vec::new();
		}

		let mut output = vec![token];
		loop {
			let next = self.map[&current].next();

			current = current[1..self.order.get()].to_vec();
			current.push(next.clone());
			if let Some(next) = next {
				output.push(next);
			}

			if current[self.order.get() - 1].is_none() {
				break;
			}
		}

		output
	}

	/// Produces an infinite iterator of generated token collections.
	pub const fn iter(&self) -> InfiniteChainIterator<T> {
		InfiniteChainIterator { chain: self }
	}

	/// Produces an iterator for the specified number of generated token collections.
	pub const fn iter_for(&self, size: usize) -> SizedChainIterator<T> {
		SizedChainIterator { chain: self, size }
	}
}

impl Chain<String> {
	/// Feeds a string of text into the chain.
	pub fn feed_str(&mut self, string: &str) -> &mut Self {
		self.feed(&string.split(' ').map(ToOwned::to_owned).collect::<Vec<_>>())
	}

	/// Generates a random string of text.
	pub fn generate_str(&self) -> String {
		self.generate().join(" ")
	}

	/// Generates a random string of text starting with the desired token. This returns an empty
	/// string if the token is not found.
	pub fn generate_str_from_token(&self, string: &str) -> String {
		self.generate_from_token(string.to_owned()).join(" ")
	}

	/// Produces an infinite iterator of generated strings.
	pub fn str_iter(&self) -> InfiniteChainStringIterator {
		self.iter().map(|v| v.join(" "))
	}

	/// Produces a sized iterator of generated strings.
	pub fn str_iter_for(&self, size: usize) -> SizedChainStringIterator {
		self.iter_for(size).map(|v| v.join(" "))
	}
}

/// A sized iterator over a Markov chain of strings.
pub type SizedChainStringIterator<'a> =
	Map<SizedChainIterator<'a, String>, fn(Vec<String>) -> String>;

/// A sized iterator over a Markov chain.
pub struct SizedChainIterator<'a, T: Chainable + 'a> {
	chain: &'a Chain<T>,
	size: usize,
}

impl<'a, T> Iterator for SizedChainIterator<'a, T>
where
	T: Chainable + 'a,
{
	type Item = Vec<T>;
	fn next(&mut self) -> Option<Vec<T>> {
		if self.size > 0 {
			self.size -= 1;
			Some(self.chain.generate())
		} else {
			None
		}
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		(self.size, Some(self.size))
	}
}

/// An infinite iterator over a Markov chain of strings.
pub type InfiniteChainStringIterator<'a> =
	Map<InfiniteChainIterator<'a, String>, fn(Vec<String>) -> String>;

/// An infinite iterator over a Markov chain.
pub struct InfiniteChainIterator<'a, T: Chainable + 'a> {
	chain: &'a Chain<T>,
}

impl<'a, T> Iterator for InfiniteChainIterator<'a, T>
where
	T: Chainable + 'a,
{
	type Item = Vec<T>;
	fn next(&mut self) -> Option<Vec<T>> {
		Some(self.chain.generate())
	}
}

/// A collection of states for the Markov chain.
trait States<T: PartialEq> {
	/// Adds a state to this states collection.
	fn add(&mut self, token: Token<T>);

	/// Gets the next state from this collection of states.
	fn next(&self) -> Token<T>;
}

impl<T> States<T> for HashMap<Token<T>, usize>
where
	T: Chainable,
{
	fn add(&mut self, token: Token<T>) {
		match self.entry(token) {
			Entry::Occupied(mut e) => *e.get_mut() += 1,
			Entry::Vacant(e) => {
				e.insert(1);
			}
		}
	}

	fn next(&self) -> Token<T> {
		let mut sum = 0;
		for &value in self.values() {
			sum += value;
		}
		let mut rng = thread_rng();
		let cap = rng.gen_range(0..sum);
		sum = 0;
		for (key, &value) in self {
			sum += value;
			if sum > cap {
				return key.clone();
			}
		}
		unreachable!("The random number generator failed.")
	}
}

#[cfg(test)]
mod test {
	use {super::Chain, std::num::NonZeroUsize};

	#[test]
	fn new() {
		Chain::<u8>::new();
		Chain::<String>::new();
	}

	#[test]
	fn is_empty() {
		let mut chain = Chain::<u8>::new();
		assert!(chain.is_empty());
		chain.feed(vec![1, 2, 3]);
		assert!(!chain.is_empty());
	}

	#[test]
	fn feed() {
		let mut chain = Chain::<u8>::new();
		chain.feed(vec![3, 5, 10]).feed(vec![5, 12]);
	}

	#[test]
	fn generate() {
		let mut chain = Chain::<u8>::new();
		chain.feed(vec![3, 5, 10]).feed(vec![5, 12]);
		let v = chain.generate();
		assert!([vec![3, 5, 10], vec![3, 5, 12], vec![5, 10], vec![5, 12]].contains(&v));
	}

	#[test]
	fn generate_for_higher_order() {
		let mut chain = Chain::<u8>::of_order(NonZeroUsize::new(2).unwrap());
		chain.feed(vec![3, 5, 10]).feed(vec![2, 3, 5, 12]);
		let v = chain.generate();
		assert!([
			vec![3, 5, 10],
			vec![3, 5, 12],
			vec![2, 3, 5, 10],
			vec![2, 3, 5, 12]
		]
		.contains(&v));
	}

	#[test]
	fn generate_from_token() {
		let mut chain = Chain::<u8>::new();
		chain.feed(vec![3, 5, 10]).feed(vec![5, 12]);
		let v = chain.generate_from_token(5);
		assert!([vec![5, 10], vec![5, 12]].contains(&v));
	}

	#[test]
	fn generate_from_unfound_token() {
		let mut chain = Chain::<u8>::new();
		chain.feed(vec![3, 5, 10]).feed(vec![5, 12]);
		let v: Vec<_> = chain.generate_from_token(9);
		assert!(v.is_empty());
	}

	#[test]
	fn iter() {
		let mut chain = Chain::<u8>::new();
		chain.feed(vec![3, 5, 10]).feed(vec![5, 12]);
		assert_eq!(chain.iter().size_hint().1, None);
	}

	#[test]
	fn iter_for() {
		let mut chain = Chain::<u8>::new();
		chain.feed(vec![3, 5, 10]).feed(vec![5, 12]);
		assert_eq!(chain.iter_for(5).count(), 5);
	}

	#[test]
	fn feed_str() {
		let mut chain = Chain::new();
		chain.feed_str("I like cats and dogs");
	}

	#[test]
	fn generate_str() {
		let mut chain = Chain::new();
		chain.feed_str("I like cats").feed_str("I hate cats");
		assert!(["I like cats", "I hate cats"].contains(&&chain.generate_str()[..]));
	}

	#[test]
	fn generate_str_from_token() {
		let mut chain = Chain::new();
		chain.feed_str("I like cats").feed_str("cats are cute");
		assert!(["cats", "cats are cute"].contains(&&chain.generate_str_from_token("cats")[..]));
	}

	#[test]
	fn generate_str_from_token_higher_order() {
		let mut chain = Chain::of_order(NonZeroUsize::new(2).unwrap());
		chain.feed_str("I like cats").feed_str("cats are cute");
		println!("{:?}", chain.generate_str_from_token("cats"));
		assert!(["cats", "cats are cute"].contains(&&chain.generate_str_from_token("cats")[..]));
	}

	#[test]
	fn generate_str_from_unfound_token() {
		let mut chain = Chain::new();
		chain.feed_str("I like cats").feed_str("cats are cute");
		assert_eq!(chain.generate_str_from_token("test"), "");
	}

	#[test]
	fn str_iter() {
		let mut chain = Chain::new();
		chain.feed_str("I like cats and I like dogs");
		assert_eq!(chain.str_iter().size_hint().1, None);
	}

	#[test]
	fn str_iter_for() {
		let mut chain = Chain::new();
		chain.feed_str("I like cats and I like dogs");
		assert_eq!(chain.str_iter_for(5).count(), 5);
	}
}
