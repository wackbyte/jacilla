use {
	crate::{
		settings::Settings,
		utils::{Filed, Quotes},
	},
	jacildata::Data,
	rand::{rngs::SmallRng, Rng, SeedableRng},
	serenity::{
		framework::standard::{
			macros::{command, group, hook},
			Args, CommandResult, DispatchError,
		},
		gateway::ActivityData,
		model::{
			channel::{Message, ReactionType},
			id::{ChannelId, EmojiId},
		},
		prelude::*,
	},
	std::time::Duration,
	tokio::task::JoinHandle,
};

pub struct DataContainer;

impl TypeMapKey for DataContainer {
	type Value = Filed<Data>;
}

pub struct SettingsContainer;

impl TypeMapKey for SettingsContainer {
	type Value = Filed<Settings>;
}

pub struct QuotesContainer;

impl TypeMapKey for QuotesContainer {
	type Value = Quotes;
}

#[group]
#[commands(help, jop, reload, get, set)]
pub struct General;

#[command]
pub async fn help(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
	let _ = msg.channel_id.say(ctx, "I'm here to help").await;
	Ok(())
}

/// the equivalent of a ping command.
#[command]
pub async fn jop(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
	let _ = msg.channel_id.say(ctx, "JOP.").await;
	Ok(())
}

#[command]
pub async fn reload(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
	let mut shared = ctx.data.write().await;

	log::info!("Reloading data...");
	{
		let built = {
			let data = shared.get_mut::<DataContainer>().unwrap();
			data.reload().await?;

			log::info!("Rebuilding quotes...");
			Quotes::build(data)
		};

		let quotes = shared.get_mut::<QuotesContainer>().unwrap();
		*quotes = built;
	}
	log::info!("Reloading settings...");
	{
		let settings = shared.get_mut::<SettingsContainer>().unwrap();
		settings.reload().await?;
	}

	let _ = msg.channel_id.say(ctx, "We won").await;

	Ok(())
}

#[command]
pub async fn get(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
	let shared = ctx.data.read().await;
	let settings = shared.get::<SettingsContainer>().unwrap();

	match args.single::<String>()?.as_str() {
		"listening" => {
			let _ = msg
				.channel_id
				.say(
					ctx,
					if let Some(listening) = &settings.listening {
						format!(
							"I am currently listening to \"{}\". Please make it stop",
							listening
						)
					} else {
						"I cannot hear👂".to_owned()
					},
				)
				.await;
		}
		"chance" => {
			let _ = msg
				.channel_id
				.say(
					ctx,
					format!(
						"There is a 1/{} chance of preparation. Keep your eyes peeled",
						settings.prepare
					),
				)
				.await;
		}
		"prepare" => {
			let _ = msg
				.channel_id
				.say(
					ctx,
					if let Some(guild_id) = msg.guild_id {
						if let Some(guild_settings) = settings.guilds.get(&guild_id) {
							if let Some(prepare) = guild_settings.prepare {
								format!("this thing: {prepare}")
							} else {
								"no mojo for you".to_owned()
							}
						} else {
							"this guild is not configured".to_owned()
						}
					} else {
						"not a guild idiot".to_owned()
					},
				)
				.await;
		}
		"interval" => {
			let _ = msg
				.channel_id
				.say(
					ctx,
					if let Some(secrets) = settings.channels.get(&msg.channel_id) {
						format!(
							"I will reveal a secret every {} seconds.",
							secrets.interval.as_secs()
						)
					} else {
						"this channel is not configured.".to_owned()
					},
				)
				.await;
		}
		key => {
			let _ = msg
				.channel_id
				.say(ctx, format!("unknown setting \"{}\"..", key))
				.await;
		}
	}

	Ok(())
}

#[command]
pub async fn set(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
	let mut shared = ctx.data.write().await;
	let settings = shared.get_mut::<SettingsContainer>().unwrap();

	match args.single::<String>()?.as_str() {
		"listening" => {
			let listening = args.single_quoted::<String>()?;

			ctx.set_activity(Some(ActivityData::listening(listening.clone())));
			settings.listening = Some(listening.clone());
			log::info!("Listening to \"{}\".", listening);

			let _ = msg
				.channel_id
				.say(
					ctx,
					format!("i am now listening to \"{}\". Thanks I guess", listening),
				)
				.await;
		}
		"chance" => {
			if let Ok(chance) = args.parse::<u32>() {
				let chance = chance.max(1);

				settings.prepare = chance;
				log::info!("Prepare chance was set to {}.", chance);

				let _ = msg
					.channel_id
					.say(ctx, format!("Preparing one in {} messages", chance))
					.await;
			} else {
				let _ = msg.channel_id.say(ctx, "Not a number").await;
			}
		}
		"prepare" => {
			if let Ok(prepare_id) = args.parse::<u64>() {
				let prepare = EmojiId::new(prepare_id);

				if let Some(guild_id) = msg.guild_id {
					let guild_settings = settings.guild(guild_id);
					guild_settings.prepare = Some(prepare);
					log::info!(
						"Guild {}'s prepare settings was set to {}.",
						guild_id,
						prepare
					);

					let _ = msg.channel_id.say(ctx, "Hes here").await;
				} else {
					let _ = msg.channel_id.say(ctx, "NOT a guild. Stop.").await;
				}
			} else {
				let _ = msg.channel_id.say(ctx, "that isnt an emoj🤙").await;
			}
		}
		"interval" => {
			let interval = args.single::<u16>()?.max(1);

			let channel_settings = settings.channel(msg.channel_id);
			channel_settings.interval = Duration::from_secs(interval.into());
			log::info!(
				"Channel {}'s interval setting was set to {} seconds.",
				msg.channel_id,
				interval
			);

			let _ = msg
				.channel_id
				.say(
					ctx,
					format!(
						"i will reveal a secret once every {} seconds in this channel......",
						interval
					),
				)
				.await;
		}
		key => {
			let _ = msg
				.channel_id
				.say(ctx, format!("unknown setting \"{}\"..", key))
				.await;
		}
	}

	log::info!("Saving settings...");
	settings.save().await?;

	Ok(())
}

#[hook]
pub async fn before(_ctx: &Context, msg: &Message, command_name: &str) -> bool {
	log::info!(
		"Running command `{}` for user `{}`",
		command_name,
		msg.author.tag()
	);
	true
}

#[hook]
pub async fn after(
	_ctx: &Context,
	_msg: &Message,
	command_name: &str,
	command_result: CommandResult,
) {
	match command_result {
		Ok(_) => log::info!("Processed command `{}`", command_name),
		Err(why) => log::error!("Command `{}` failed: {}", command_name, why),
	}
}

#[hook]
pub async fn normal_message(ctx: &Context, msg: &Message) {
	// Is the message from a guild?
	if let Some(guild_id) = msg.guild_id {
		let shared = ctx.data.read().await;
		let settings = shared.get::<SettingsContainer>().unwrap();

		// Is this guild configured?
		if let Some(guild_settings) = settings.guilds.get(&guild_id) {
			// Does it have a prepare emoji set?
			if let Some(prepare) = guild_settings.prepare {
				// Is ths channel configured?
				if let Some(channel_settings) = settings.channels.get(&msg.channel_id) {
					// Is it preparing, and does the RNG say yes?
					if channel_settings.preparing && will_you(settings.prepare) {
						// Then react.
						let _ = msg
							.react(
								ctx,
								ReactionType::Custom {
									id: prepare,
									animated: false,
									name: Some("prepare".to_owned()),
								},
							)
							.await;
					}
				}
			}
		}
	}

	// Only handle phrases from real users.
	// TODO: Webhooks??
	if !msg.author.bot {
		match msg.content.trim().to_lowercase().as_str() {
			"prepare" | "prepare." => {
				let mut shared = ctx.data.write().await;
				let settings = shared.get_mut::<SettingsContainer>().unwrap();

				let secrets = settings.channel(msg.channel_id);
				secrets.preparing = true;
				log::info!(
					"Channel {} prepared by `{}`",
					msg.channel_id,
					msg.author.tag()
				);

				log::info!("Saving settings...");
				if let Err(error) = settings.save().await {
					log::error!("Failed to save settings: {}", error);
				}
			}
			"unprepare" | "unprepare." => {
				let mut shared = ctx.data.write().await;
				let settings = shared.get_mut::<SettingsContainer>().unwrap();

				let secrets = settings.channel(msg.channel_id);
				secrets.preparing = false;
				log::info!(
					"Channel {} unprepared by `{}`",
					msg.channel_id,
					msg.author.tag()
				);

				log::info!("Saving settings...");
				if let Err(error) = settings.save().await {
					log::error!("Failed to save settings: {}", error);
				}
			}
			"reveal secret" | "reveal secret." => {
				let shared = ctx.data.read().await;
				let quotes = &shared.get::<QuotesContainer>().unwrap().data;

				let index = SmallRng::from_entropy().gen_range(0..quotes.len());
				let quote = &quotes[index];

				log::info!(
					"Secret revealed manually by `{}` in channel {}: {}",
					msg.author.tag(),
					msg.channel_id,
					quote
				);

				let _ = msg.channel_id.say(ctx, quote).await;
			}
			"lock secrets" | "lock secrets." => {
				let mut shared = ctx.data.write().await;
				let settings = shared.get_mut::<SettingsContainer>().unwrap();

				let secrets = settings.channel(msg.channel_id);
				if !secrets.unlocked {
					let _ = msg
						.channel_id
						.say(ctx, "secrets are already locked....")
						.await;
				}
				secrets.unlocked = false;
				log::info!(
					"Secrets locked by `{}` in channel {}.",
					msg.author.tag(),
					msg.channel_id
				);

				log::info!("Saving settings...");
				if let Err(error) = settings.save().await {
					log::error!("Failed to save settings: {}", error);
				}

				let _ = msg.channel_id.say(ctx, "Secrets locked.").await;
			}
			"unlock secrets" | "unlock secrets." => {
				let mut shared = ctx.data.write().await;
				let settings = shared.get_mut::<SettingsContainer>().unwrap();

				let secrets = settings.channel(msg.channel_id);
				if secrets.unlocked {
					let _ = msg
						.channel_id
						.say(ctx, "secrets are already unlocked.")
						.await;
				} else {
					secrets.unlocked = true;
					log::info!(
						"Secrets unlocked by `{}` in channel {}.",
						msg.author.tag(),
						msg.channel_id
					);

					spawn_secret_unlocker(ctx.clone(), msg.channel_id);

					log::info!("Saving settings...");
					if let Err(error) = settings.save().await {
						log::error!("Failed to save settings: {}", error);
					}

					let _ = msg.channel_id.say(ctx, "Secrets unlocked.").await;
				}
			}
			"i'm done" | "i'm done." | "im done" | "im done." => {
				log::info!("I'm done 😂😂😂");
				let _ = msg.channel_id.say(ctx, "I'm done 😂😂😂").await;
				let _ = msg.channel_id.say(ctx, "https://cdn.discordapp.com/attachments/794271766003843112/802585297853349898/IMG_20210122_230904.jpg").await;
			}
			"soos culu's" | "soos culu's." | "culu's" | "culu's." => {
				log::info!("soos culu's");
				let _ = msg.channel_id.say(ctx, "soos culu's.").await;
				let _ = msg.channel_id.say(ctx, "https://cdn.discordapp.com/attachments/794271766003843112/802960715484626964/image0.png").await;
			}
			"'s" | "'s." => {
				log::info!("'s.");
				let _ = msg.channel_id.say(ctx, "https://cdn.discordapp.com/attachments/794271766003843112/802962222309572629/1611457583844.png").await;
			}
			"hogs are hype" | "hogs can help" | "hogs can hear" | "hogs can't hear"
			| "hogs cant hear" | "hogs can hype" | "hogs are real" | "hogs aren't real"
			| "hogs arent real" | "hogs need help" => {
				log::info!("Hog detected");
				let _ = msg.channel_id.say(ctx, "TRUE..").await;
			}
			_ => {}
		}
	}
}

#[hook]
pub async fn dispatch_error(ctx: &Context, msg: &Message, error: DispatchError, _command: &str) {
	if let DispatchError::Ratelimited(info) = error {
		// They will only receive ONE (1) warning.
		if info.is_first_try {
			let _ = msg
				.channel_id
				.say(
					ctx,
					format!(
						"Idiots in my life... try again in {} seconds.",
						info.as_secs()
					),
				)
				.await;
		}
	}
}

pub fn will_you(chance: u32) -> bool {
	SmallRng::from_entropy().gen_range(0..chance) == 0
}

pub fn spawn_secret_unlocker(ctx: Context, channel: ChannelId) -> JoinHandle<()> {
	log::debug!("Secrets unlocker task spawned for channel {}.", channel);

	let mut rng = SmallRng::from_entropy();
	tokio::spawn(async move {
		loop {
			// We avoid keeping the data lock while sleeping.
			let mut interval = {
				let shared = ctx.data.read().await;
				if let Some(channel_settings) = shared
					.get::<SettingsContainer>()
					.unwrap()
					.channels
					.get(&channel)
				{
					if !channel_settings.unlocked {
						break;
					}

					let quotes = &shared.get::<QuotesContainer>().unwrap().data;
					let index = rng.gen_range(0..quotes.len());
					let quote = &quotes[index];

					log::debug!("Secret revealed in channel {}: \"{}\"", channel, quote);
					let _ = channel.say(&ctx, quote).await;

					let mut interval = tokio::time::interval(channel_settings.interval);
					interval.tick().await;
					interval
				} else {
					break;
				}
			};
			interval.tick().await;
		}
		log::debug!("Secrets unlocker task ended in channel {}.", channel);
	})
}
