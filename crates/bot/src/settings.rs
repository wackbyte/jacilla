use {
	serde::{Deserialize, Serialize},
	serenity::model::id::{ChannelId, EmojiId, GuildId},
	std::{collections::HashMap, time::Duration},
};

fn default_prepare() -> u32 {
	10
}

/// Global settings.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Settings {
	/// The listening text.
	#[serde(default)]
	pub listening: Option<String>,
	/// The preparing chance.
	#[serde(default = "default_prepare")]
	pub prepare: u32,
	/// Guild settings.
	#[serde(default)]
	pub guilds: HashMap<GuildId, GuildSettings>,
	/// Channel settings.
	#[serde(default)]
	pub channels: HashMap<ChannelId, ChannelSettings>,
}

impl Settings {
	/// Get or initialize guild settings.
	pub fn guild(&mut self, id: GuildId) -> &mut GuildSettings {
		self.guilds.entry(id).or_default()
	}

	/// Get or initialize channel settings.
	pub fn channel(&mut self, id: ChannelId) -> &mut ChannelSettings {
		self.channels.entry(id).or_default()
	}
}

/// Settings for individual guilds.
#[derive(Clone, Debug, Default, Eq, PartialEq, Serialize, Deserialize)]
pub struct GuildSettings {
	/// The prepare emoji.
	pub prepare: Option<EmojiId>,
}

/// Settings for individual channels.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ChannelSettings {
	/// Whether secrets are unlocked or not.
	pub unlocked: bool,
	/// Whether we're preparing or not.
	pub preparing: bool,
	/// The secret unlockage interval.
	pub interval: Duration,
}

impl Default for ChannelSettings {
	fn default() -> Self {
		Self {
			unlocked: false,
			preparing: false,
			interval: Duration::from_secs(120),
		}
	}
}
