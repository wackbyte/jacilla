use {
	anyhow::{Context as ErrorContext, Result},
	jacildata::Data,
	serde::{Deserialize, Serialize},
	std::{
		ops::{Deref, DerefMut},
		path::Path,
	},
	tokio::{
		fs::File,
		io::{AsyncReadExt, AsyncWriteExt},
	},
};

/// Cached quotes.
pub struct Quotes {
	pub data: Vec<String>,
}

impl Quotes {
	pub fn build(data: &Data) -> Self {
		Self {
			data: data
				.streams
				.iter()
				.filter_map(|stream| stream.quotes.clone().map(IntoIterator::into_iter))
				.flatten()
				.chain(data.tweets.clone())
				.collect::<Vec<String>>(),
		}
	}
}

/// JSON data loaded from a file.
#[derive(Debug, Clone)]
pub struct Filed<T> {
	/// The path to the file where the data is stored.
	///
	/// We use a path rather than an actual file so if the original file is moved,
	/// this will still point where we actually want it to.
	pub path: Box<Path>,
	/// The data in-memory.
	pub data: T,
}

impl<T> Filed<T>
where
	T: for<'de> Deserialize<'de>,
{
	/// Initialize the data after loading it from a file.
	pub async fn load<P>(path: P) -> Result<Self>
	where
		P: AsRef<Path>,
	{
		let path = path.as_ref();
		log::debug!("Loading file at {:?}.", &path);

		let mut file = File::open(&path)
			.await
			.with_context(|| format!("Failed to open the file at {:?}.", &path))?;
		let mut text = String::new();
		file.read_to_string(&mut text)
			.await
			.with_context(|| format!("Failed to load the file at {:?}.", &path))?;

		Ok(Self {
			path: path.into(),
			data: json::from_str(&text).context("Failed to deserialize the loaded data.")?,
		})
	}

	/// Reload the data from the file.
	pub async fn reload(&mut self) -> Result<()> {
		log::debug!("Reloading file at {:?}.", &self.path);

		let mut file = File::open(&self.path)
			.await
			.with_context(|| format!("Failed to reopen the file at {:?}.", &self.path))?;
		let mut text = String::new();
		file.read_to_string(&mut text)
			.await
			.with_context(|| format!("Failed to reload the file at {:?}.", &self.path))?;
		self.data = json::from_str(&text).context("Failed to deserialize the reloaded data.")?;

		Ok(())
	}
}

impl<T> Filed<T>
where
	T: Serialize,
{
	/// Write the data to the file.
	pub async fn save(&mut self) -> Result<()> {
		log::debug!("Saving file at {:?}.", &self.path);

		let mut file = File::create(&self.path)
			.await
			.with_context(|| format!("Failed to create the file at {:?}.", &self.path))?;
		let text = json::to_vec(&self.data).context("Failed to serialize the data.")?;
		file.write_all(&text)
			.await
			.with_context(|| format!("Failed to save the file at {:?}.", &self.path))
	}
}

impl<T> Deref for Filed<T> {
	type Target = T;

	fn deref(&self) -> &Self::Target {
		&self.data
	}
}

impl<T> DerefMut for Filed<T> {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.data
	}
}
