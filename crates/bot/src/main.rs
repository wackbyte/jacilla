use {
	crate::{
		framework::{DataContainer, QuotesContainer, SettingsContainer},
		utils::{Filed, Quotes},
	},
	anyhow::{bail, Context as ErrorContext, Result},
	serenity::{
		async_trait,
		framework::{standard::Configuration, StandardFramework},
		gateway::ActivityData,
		http::Http,
		model::gateway::{GatewayIntents, Ready},
		prelude::*,
	},
	std::{env, path::PathBuf},
};

mod framework;
mod settings;
mod utils;

/// The stream wouldn't be what it is without you...
struct Handler;

#[async_trait]
impl EventHandler for Handler {
	async fn ready(&self, ctx: Context, _ready: Ready) {
		log::info!("I'm in. They're hacked.");

		let shared = ctx.data.read().await;
		let settings = shared.get::<SettingsContainer>().unwrap();
		if let Some(listening) = &settings.listening {
			log::info!("Listening to \"{}\".", listening);
			ctx.set_activity(Some(ActivityData::listening(listening)));
		}

		log::info!("Spawning previously running secret unlockers...");
		for (channel, secrets) in &settings.channels {
			if secrets.unlocked {
				framework::spawn_secret_unlocker(ctx.clone(), *channel);
			}
		}
	}
}

#[tokio::main]
async fn main() -> Result<()> {
	env_logger::init_from_env(
		env_logger::Env::new()
			.filter("JACILLA_LOG")
			.write_style("JACILLA_LOG_STYLE"),
	);

	let token = env::var("JACILLA_TOKEN").context("There's no token to unlock the secrets with. Provide one with the environment variable JACILLA_TOKEN.")?;

	log::info!("Fetching some information about the bot...");
	let http = Http::new(&token);
	let id = http
		.get_current_user()
		.await
		.context("Failed to fetch information about the bot.")?
		.id;

	log::info!("Finding storage...");
	let storage_path = PathBuf::from(env::var("JACILLA_STORAGE").context("There's no place to track everyone's personal information with. Provide a path to it with the environment variable JACILLA_STORAGE.")?);
	if !storage_path.exists() {
		bail!(
			"The storage path at {:?} does not exist. Please make it exist.",
			&storage_path
		);
	} else if !storage_path.is_dir() {
		bail!("The storage path at {:?} is not a directory. How am I supposed to doxx you without a place to store your location? 😕", &storage_path);
	}

	log::info!("Finding data...");
	let data_path = storage_path.join("data.json");
	if !data_path.exists() {
		bail!("The data path at {:?} does not exist.", &storage_path);
	} else if !data_path.is_file() {
		bail!("The data path at {:?} is not a file.", &data_path);
	}

	log::info!("Finding settings...");
	let settings_path = storage_path.join("settings.json");
	if !settings_path.exists() {
		bail!("The settings path at {:?} does not exist.", &settings_path);
	} else if !settings_path.is_file() {
		bail!("The settings path at {:?} is not a file.", &settings_path);
	}

	log::info!("Loading data...");
	let data = Filed::load(data_path).await?;
	log::info!("Loading settings...");
	let settings = Filed::load(settings_path).await?;
	log::info!("Building quotes...");
	let quotes = Quotes::build(&data);

	log::info!("Initializing framework...");
	let framework = StandardFramework::new()
		.before(framework::before)
		.after(framework::after)
		.normal_message(framework::normal_message)
		.on_dispatch_error(framework::dispatch_error)
		.group(&framework::GENERAL_GROUP);
	framework.configure(
		Configuration::new()
			.with_whitespace(true)
			.on_mention(Some(id))
			.prefix("~")
			.ignore_bots(false),
	);

	log::info!("Starting client...");
	let mut client = Client::builder(&token, GatewayIntents::all())
		.event_handler(Handler)
		.framework(framework)
		.type_map_insert::<DataContainer>(data)
		.type_map_insert::<SettingsContainer>(settings)
		.type_map_insert::<QuotesContainer>(quotes)
		.await
		.context("Failed to get inside your house.")?;

	client.start().await.context("Failed to fly by.")
}
