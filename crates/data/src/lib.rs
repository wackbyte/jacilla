#![warn(clippy::pedantic)]

use serde::{Deserialize, Serialize};

pub use time::Date;

/// A question from one of Jacilla's quizzes.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Question {
	// TODO: Start at 0 or 1?
	/// The question number. Starts at 1.
	pub number: u16,
	/// The text of the question.
	pub content: String,
	/// If the question was multiple choice, include the answers.
	#[serde(default)]
	pub answers: Option<Vec<String>>,
}

/// A Jacilla stream.
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Stream {
	// TODO: Start at 0 or 1?
	/// The chronological ordering of the stream. Starts at 0.
	pub index: u16,
	/// The title the stream was broadcasted under. This may be empty, as it is with the first stream.
	pub title: String,
	/// The day the stream was broadcasted on.
	pub date: Date,
	/// A list of every bit of text shown if any.
	#[serde(default)]
	pub quotes: Option<Vec<String>>,
	/// A list of every question asked if any.
	#[serde(default)]
	pub questions: Option<Vec<Question>>,
	/// The YouTube video ID of the archived VOD if it exists.
	#[serde(default)]
	pub video: Option<String>,
}

/// The Jacilla dataset. It includes data regarding both streams and tweets.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Data {
	/// A list of Jacilla streams.
	pub streams: Vec<Stream>,
	// TODO: create a dedicated Tweet struct and store more information?
	/// A list of Jacilla tweets.
	pub tweets: Vec<String>,
}
