# Notes

- I found `Water [...] Mode`'s exact space count through a recreation of the scene in OBS. It should be accurate.
- `We're a group of entrepreneurs who (?) experience in the way e(?) see and g(?)` is incomplete and it's unlikely it'll ever be known.
- Upside-down quotes such as `dlǝɥ oʇ ǝɹǝɥ` currently use the funny silly upside-down characters.
- All known tweets are collected, even if they have been deleted.
  - Of course, tweets that were deleted prior to the beginning of the Jacilla documentation cannot be known.
    - Unless they're on the Internet Archive by some chance, but that would be far too much work.
- Those silly funny apostrophes/quotes are replaced with the normal straight ones.
