{
  description = "jacilla";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    naersk = {
      url = "github:nmattia/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, flake-utils, fenix, naersk, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        lib = pkgs.lib;

        naerskLib = naersk.lib.${system};
        toolchain = fenix.packages.${system}.stable.withComponents [
          "cargo"
          "clippy"
          "rustc"
          "rustfmt"
        ];
      in
      rec {
        packages.jacilla = (naerskLib.override {
          cargo = toolchain;
          rustc = toolchain;
        }).buildPackage {
          src = ./.;
        };
        defaultPackage = packages.jacilla;

        apps.jacilla = flake-utils.lib.mkApp {
          drv = packages.jacilla;
        };
        defaultApp = apps.jacilla;

        overlay = self: super: {
          jacilla = packages.jacilla;
        };

        formatter = pkgs.nixpkgs-fmt;

        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            toolchain
          ];

          JACILLA_LOG = "jacilbot=debug";
          JACILLA_STORAGE = "./testing";
        };
      }
    );
}
