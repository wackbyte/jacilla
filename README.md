# jacilla

## Who is Jacilla?

Jacilla is a Twitter user ([@jaciIla](https://twitter.com/jaciIla)) who makes absurd and non sequitur tweets similar to that of [@dril](https://en.wikipedia.org/wiki/Dril). During a period between December 2020 and March 2021, they streamed to Twitch ([@jacilla](https://twitch.tv/jacilla)), their streams being similarly surreal to their tweets. They were produced in OBS using basic transitions, text sources, and image sources. Occasionally, elements would be "animated" by dragging around the scene rather than by transition. An unofficial YouTube channel named [Jacilla Vods](https://www.youtube.com/@jacillavods5145) contains archives of these streams as the videos on Twitch are already long-gone.

## What is this project?

This project provides several Jacilla-related things:

- [`jacilbot`](crates/bot): a Discord bot which has the primary function of periodically repeating "quotes" by Jacilla.
- [`jacilchain`](crates/chain): a tool to generate quotes using a [Markov chain](https://en.wikipedia.org/wiki/Markov_chain).
- [`jacildata`](crates/data): a "database" (handwritten JSON) providing a list of streams and tweets.

## Attribution

- `jacilchain` was adapted from the public domain [`markov`](https://github.com/aatxe/markov) crate.
- The compilation of "quotes" from the streams and Twitter was a group effort, but it was completed primarily by Pehp. Thank you.

## License

I release this project into the public domain using the [Unlicense](UNLICENSE) with the following exception: the content compiled within the database is created by Jacilla.
